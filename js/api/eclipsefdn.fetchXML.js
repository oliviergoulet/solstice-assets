/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *  Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// @todo: consider replacing jquery with a native solution
import jQuery from 'jquery';
import 'core-js/modules/es.string.pad-start';

export const filterHTML = (html) => {
  const txt = document.createElement('div');
  txt.innerHTML = html;
  return txt.textContent;
};

export const fetchXML = (url) => {
  return new Promise(function (resolve, reject) {
    jQuery.ajax({
      type: 'GET',
      dataType: 'xml',
      async: true,
      url: url,
    })
      .done(function (data) {
        resolve(data);
      })
      .fail(function () {
        reject();
      });
  });
};


